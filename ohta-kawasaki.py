#!/usr/bin/env python
# -*- coding: utf-8 -*-

from dolfin import *
from petsc4py import PETSc
import sys
from math import log
import gc

monitor_executed = False # a flag to say if we've executed the monitor before or not

def main():
  # The average value of the solution u, which is conserved
  m = 0.4

  # Set the necessary PETSc options for our solver
  args = [sys.argv[0]] + """
                               --petsc.ok_snes_monitor
                               --petsc.ok_snes_lag_preconditioner -1
                               --petsc.ok_snes_no_convergence_test
                               --petsc.ok_snes_max_it 1

                               --petsc.ok_ksp_converged_reason
                               --petsc.ok_ksp_type gmres
                               --petsc.ok_ksp_monitor
                               --petsc.ok_ksp_rtol 1.0e-8

                               --petsc.ok_pc_type fieldsplit
                               --petsc.ok_pc_fieldsplit_type schur
                               --petsc.ok_pc_fieldsplit_schur_factorization_type lower
                               --petsc.ok_pc_fieldsplit_schur_precondition self

                               --petsc.ok_fieldsplit_0_ksp_type chebyshev
                               --petsc.ok_fieldsplit_0_ksp_max_it 10
                               --petsc.ok_fieldsplit_0_pc_type sor

                               --petsc.ok_fieldsplit_1_ksp_type richardson
                               --petsc.ok_fieldsplit_1_ksp_max_it 2
                               --petsc.ok_fieldsplit_1_pc_type mat
                               --petsc.ok_fieldsplit_1_hats_pc_type hypre
                               --petsc.ok_fieldsplit_1_hats_pc_hypre_type boomeramg
                               --petsc.ok_fieldsplit_1_hats_pc_hypre_boomeramg_no_CF
                               --petsc.ok_fieldsplit_1_hats_pc_hypre_boomeramg_agg_nl 1
                               --petsc.ok_fieldsplit_1_hats_pc_hypre_boomeramg_coarsen_type HMIS
                               --petsc.ok_fieldsplit_1_hats_pc_hypre_boomeramg_interp_type ext+i
                               --petsc.ok_fieldsplit_1_hats_pc_hypre_boomeramg_P_max 4
                               --petsc.ok_fieldsplit_1_hats_pc_hypre_boomeramg_agg_num_paths 2
                               --petsc.ok_fieldsplit_1_hats_ksp_type richardson
                               --petsc.ok_fieldsplit_1_hats_ksp_max_it 5
                               """.split()
  parameters.parse(args)

  # Change this to 2 to solve a two-dimensional problem
  dim = 3

  # Class for interfacing with the Newton solver
  class NonlinearEquation(NonlinearProblem):
      def __init__(self, a, L):
          NonlinearProblem.__init__(self)
          self.L = L
          self.a = a
      def F(self, b, x):
          assemble(self.L, tensor=b)
      def J(self, A, x):
          assemble(self.a, tensor=A)

  # Model parameters
  eps    = 0.02    # interfacial thickness
  sigma  = 100     # nonlocal energy coefficient
  dt     = eps**2  # time step
  theta  = 0.5     # implicitness parameter

  print "(ε, σ, Δt, θ): ", (eps, sigma, dt, theta)

  # Form compiler options
  parameters["form_compiler"]["optimize"]     = True
  parameters["form_compiler"]["cpp_optimize"] = True
  parameters["form_compiler"]["representation"] = "uflacs"
  parameters["form_compiler"]["cpp_optimize_flags"] = "-O3 -ffast-math -march=native"

  if has_parmetis():
    parameters["mesh_partitioner"] = "ParMETIS"

  # Generate the mesh to achieve an approximate target dofs per core, by
  # generating an initial coarse mesh and then refining it.  This is much faster
  # in parallel than making the large mesh directly.
  t0 = Timer("Mesh construction")

  N = 50
  if dim == 2:
    mesh = UnitSquareMesh(N, N)
  elif dim == 3:
    mesh = UnitCubeMesh(N, N, N)

  r = int(log(MPI.size(mpi_comm_world()), 2**dim))
  for i in range(r):
    print "Refinement %s ..." % i
    mesh = refine(mesh, redistribute=False)
  del t0

  t0 = Timer("FunctionSpace construction")
  print "Defining function space"
  Z = VectorFunctionSpace(mesh, "Lagrange", 1, dim=2)
  del t0
  gc.collect()

  print "Degrees of freedom: ", Z.dim()
  print "Degrees of freedom per core: ", Z.dim()/MPI.size(mpi_comm_world())

  # Define trial and test functions
  dz      = TrialFunction(Z)
  (v, q)  = TestFunctions(Z)

  # Define functions
  z   = Function(Z)  # current solution
  z0  = Function(Z)  # solution from previous converged step

  # Split mixed functions
  (du, dw) = split(dz)
  (u, w)   = split(z)
  (u0, w0) = split(z0)

  # Variables used in the theta timestepping scheme
  u_mid = (1-theta)*u0 + theta*u
  w_mid = (1-theta)*w0 + theta*w

  # Create intial conditions and interpolate
  print "Interpolating/computing initial conditions"
  t0 = Timer("Initial condition interpolation/computation")

  # Use compiled C++ code to generate the initial condition.
  # This is much faster than using a Python Expression.
  ic_code = """
#include <cstdlib>

class InitialCondition : public Expression
{
    public:

    void eval(Array<double>& values, const Array<double>& x) const
    {
        double ptb;
        if (%(dim)s == 2) ptb = cos(2*pi*x[0])*cos(2*pi*x[1]);
        if (%(dim)s == 3) ptb = cos(2*pi*x[0])*cos(2*pi*x[1])*cos(2*pi*x[2]);
        values[0] = %(m)s + 0.02*ptb;

        if (%(dim)s == 2) values[1] = 0.16*pi*pi*%(eps)s*%(eps)s*cos(2*pi*x[0])*cos(2*pi*x[1]) + (%(m)s + 0.02*cos(2*pi*x[0])*cos(2*pi*x[1]))*(pow(%(m)s + 0.02*cos(2*pi*x[0])*cos(2*pi*x[1]), 2) - 1);
        if (%(dim)s == 3) values[1] = 0.24*pi*pi*%(eps)s*%(eps)s*cos(2*pi*x[0])*cos(2*pi*x[1])*cos(2*pi*x[2]) + (%(m)s + 0.02*cos(2*pi*x[0])*cos(2*pi*x[1])*cos(2*pi*x[2]))*(pow(%(m)s + 0.02*cos(2*pi*x[0])*cos(2*pi*x[1])*cos(2*pi*x[2]), 2) - 1);
    }

    std::size_t value_rank() const
    {
        return 1;
    }

    std::size_t value_dimension(size_t i) const
    {
        return 2;
    }
};
""" % dict(dim=dim, eps=eps, m=m)
  InitialCondition = Expression(ic_code, element=Z.ufl_element())
  z.interpolate(InitialCondition)

  # Delete as many objects as possible to free up memory
  del t0, InitialCondition
  print "Done interpolating"

  # Define the chemical potential df/du
  dfdu = u**3 - u

  # Weak statement of the equations
  L0 = u*v*dx - u0*v*dx + dt*dot(grad(w_mid), grad(v))*dx + dt*sigma*(u_mid - m)*v*dx
  L1 = w*q*dx - dfdu*q*dx - eps**2*dot(grad(u), grad(q))*dx
  L = L0 + L1

  # Compute directional derivative with respect to z in the direction of dz (Jacobian)
  a = derivative(L, z, dz)

  # Free up some memory
  mesh.topology().clear(0, 0)
  mesh.topology().clear(1, 0)
  mesh.topology().clear(0, 1)
  mesh.topology().clear(1, 1)

  if dim == 3:
    mesh.topology().clear(0, 2)
    mesh.topology().clear(1, 2)
    mesh.topology().clear(2, 2)
    mesh.topology().clear(3, 2)
    mesh.topology().clear(2, 0)
    mesh.topology().clear(2, 1)
    mesh.topology().clear(2, 3)

  # Create nonlinear problem and Newton solver
  problem = NonlinearEquation(a, L)
  solver = PETScSNESSolver()

  solver.parameters["report"] = False

  # Have DOLFIN set up the PETSc SNES object with the right callbacks, then
  # fetch it:
  solver.init(problem, z.vector())
  snes = solver.snes()
  snes.setOptionsPrefix("ok_")
  snes.setFromOptions()

  # Configure the Schur complement approximation.
  t0 = Timer("FieldSplit setup")
  pc = snes.ksp.pc

  # To assemble \hat{S} (which operates on w), we need to assemble a large
  # matrix that operates on (u, w) and then take the relevant submatrix.
  # This is to ensure that the degrees of freedom match up correctly.
  def extract_sub_matrix(A):
    Amat  = as_backend_type(A).mat()
    w_dofs = Z.sub(1).dofmap().dofs()
    w_is   = PETSc.IS()
    w_is.createGeneral(w_dofs)
    # More PETSc API incompatibilities
    if PETSc.Sys.getVersion()[0:2] <= (3, 7) and PETSc.Sys.getVersionInfo()['release']:
        return Amat.getSubMatrix(w_is, w_is)
    else:
        return Amat.createSubMatrix(w_is, w_is)

  # Define \hat{S} and create a KSP object to compute the action of \hat{S}^{-1}
  c = (dt * theta * eps**2)/(1 + dt * theta * sigma)
  hats = extract_sub_matrix(assemble(inner(dw, q)*dx + sqrt(c) * inner(grad(dw), grad(q))*dx))
  mat_sizes = hats.getSizes()
  ksp_hats = PETSc.KSP()
  ksp_hats.create()
  ksp_hats.setOperators(hats)
  ksp_hats.setOptionsPrefix("ok_fieldsplit_1_hats_")
  ksp_hats.setFromOptions()
  ksp_hats.setUp()
  ksp_hats.pc.setReusePreconditioner(True) # we don't need to rebuild this ever again

  # This Python class contains a mult method that implements the action of our
  # approximate Schur complement inverse.
  class SchurInv(object):
    def __init__(self):
      self.mass_op = None

    def mult(self, mat, x, y):
      tmp1 = y.duplicate()
      tmp2 = y.duplicate()

      # Compute one action of \hat{S}^{-1}
      ksp_hats.solve(x, tmp1)

      # We don't want to assemble the mass matrix again, when it is
      # already assembled as the top-left block of our Jacobian
      # (with a scaling). We can fetch this here by getting the operator
      # associated with the A^{-1} solve.
      if self.mass_op is None:
        subksps = pc.getFieldSplitSubKSP()
        self.mass_op = subksps[0].pc.getOperators()[0]
      self.mass_op.mult(tmp1, tmp2)
      # Undo the scaling in our top-left block
      tmp2.scale(1.0/(1.0 + dt*theta*sigma))

      # Compute another action of \hat{S}^{-1}
      ksp_hats.solve(tmp2, y)

  # Create a matrix-free PETSc Mat that computes the action of \tilde{S}^{-1}.
  mat_schur = PETSc.Mat()
  mat_schur.createPython(mat_sizes, SchurInv())
  mat_schur.setUp()

  # And use a SNES monitor (executed every nonlinear iteration) to set
  # the Schur complement approximation.
  def monitor(snes, its, norm):
    global monitor_executed
    pc = snes.ksp.pc
    if pc.getType() != "fieldsplit": return
    pc.setFieldSplitSchurPreType(PETSc.PC.SchurPreType.USER, mat_schur)
    if monitor_executed:
        subksps = pc.getFieldSplitSubKSP()
        subksps[0].pc.setReusePreconditioner(True)
        pc.setReusePreconditioner(True)
    monitor_executed = True

  snes.setMonitor(monitor)
  del t0

  # Variables for timestepping
  timestep = 0
  t = 0.0
  timesteps = 20 # change to e.g. 300 for convergence to steady state
  T = timesteps*dt

  # Set up I/O: disabled by default for HPC performance
  io = False
  if io:
      outputdir = "output-dofs-%s-timesteps-%s" % (Z.dim(), timesteps)
      file = XDMFFile(mpi_comm_world(), "%s/output.xdmf" % outputdir)
      file.parameters["flush_output"] = True
      file.parameters["rewrite_function_mesh"] = False
      file << (z.split()[0], t)

  # Main timestepping loop
  while (t < T):
      t += dt

      # Overwrite old value
      z0.vector()[:] = z.vector()

      # Solve the nonlinear problem for each timestep
      nls_timer = Timer("Nonlinear solver")
      snes.solve(None, as_backend_type(z.vector()).vec())
      nls_timer.stop()

      # I/O
      timestep += 1
      if timestep % 1 == 0:
        info("Output state at time %s/%s" % (t, T))

        if io: file << (z.split()[0], t)

def prevent_MPI_output():
    """
    By default, DOLFIN produces output on every process.
    If you have 10000 processes, this produces massive logfiles
    that cause difficulties for the filesystems on supercomputers.
    This routine deactivates the standard output of processes
    that are not rank 0.
    """
    if MPI.rank(mpi_comm_world()) > 0:
        # muzzle Python ...
        sys.stdout = open("/dev/null", "w")

        # and muzzle C/C++ as well
        import ctypes
        libc = ctypes.CDLL("libc.so.6")
        stdout = libc.fdopen(1, "w")
        libc.freopen("/dev/null", "w", stdout)

if __name__ == "__main__":
  prevent_MPI_output()
  main()
